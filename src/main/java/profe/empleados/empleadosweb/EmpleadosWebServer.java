package profe.empleados.empleadosweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Description;
import org.springframework.retry.annotation.EnableRetry;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;

@SpringBootApplication
@EnableRetry
public class EmpleadosWebServer {

	public static void main(String[] args) {
		SpringApplication.run(EmpleadosWebServer.class, args);
	}

}
